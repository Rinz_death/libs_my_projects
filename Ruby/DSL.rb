require '../formula'
'''
Требуемая кодировка UTF 8
Посчитай 1+1
'''
module Errors
  TYPES = 'Ошибка скорее всего связаная с кодировкой'
  SYNTAX = 'Вы ошиблись логикой написания скрипта'
end

class DSL
  def initialize(files)
    $files = files
    File.readlines($files).each do |line|
      begin
        puts line
        puts "#{line.chomp} ==>  #{eval(line.to_s)}"
      rescue TypeError
        puts Errors::TYPES
      rescue SyntaxError
        puts Errors::SYNTAX
      end
    end
  end
  # Метод выполняет расчет
  def Посчитай(*args)
    fins = ""
    args.each do |n|
      fins += n.gsub(/[A-Za-zА-Яа-я]/){}
    end
    ms = fins.split(',')
    ms.each do |x|
      puts eval(x.to_s)
    end
  end
  # Метод конвертирует в Ruby скрипт
  def Save(name)
    string = "requery 'dsl.rb'\nfile='name_file'\n"
    File.readlines($files).each do |line|
      string += line.gsub(/[Save #{name}]["]{2}/){}
    end
    File.open("#{name}.rb", 'a') do |file|
      file.puts  string+"DSL.new(file)"
    end
  end
end

DSL.new("пример.txt")